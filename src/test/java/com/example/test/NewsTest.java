package com.example.test;


import com.kroganinventiv.models.News;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.fail;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander
 * Date: 3/21/12
 * Time: 5:21 PM
 */
public class NewsTest {
    private String title;
    private Date date;
    private String subtitle;
    private String contents;

    @Before
    public void initialize() {

    }

    public static boolean exists(String urlName, int id) {
        try {
            URL url = new URL(urlName + "?id=" + id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept-Charset", "UTF-8");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK) && (con.getContentLength() != 1);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Test
    public void TestLatestNewsFetching() throws IOException {
        String url = "http://www.ratbv.ro/description-news.php?id=150";
        String id = url.split("\\?")[1].split("=")[1];
        Document doc = null;

        if (exists(url.split("\\?")[0], Integer.parseInt(id))) {
            InputStream input = new URL(url).openStream();
            doc = Jsoup.parse(input, "ISO-8859-2", "http://www.ratbv.ro/");
            doc.outputSettings().charset(Charset.forName("ISO-8859-2"));
            doc.outputSettings().escapeMode(Entities.EscapeMode.base);
        } else
            fail("Incorrect ID for resource");


        Element h4 = doc.select("h4").first();
        Element b = doc.select("b").first();
        Element i = doc.select("i").first();

        title = h4.text();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = df.parse(b.ownText());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        subtitle = i.text();

        Elements elements = doc.getElementsMatchingText("Regia Autonoma de Transport");
        for (Element e : elements) {
            contents = e.ownText();
        }

        System.out.println(title);
        System.out.println(date);
        System.out.println(subtitle);
        System.out.println(contents);

//        BufferedWriter out = new BufferedWriter(new FileWriter(new File("news.txt")));
//        out.write(contents);
//        out.close();
    }

    @Test
    public void TestGetAllNews() {
        String url = "http://www.ratbv.ro/description-news.php?id=";
        StringBuilder sb = new StringBuilder(url);
        InputStream input;

        List<News> news = new ArrayList<News>();
        for (int j = 81; j <= 150; j++) {
            sb.append(j);

            Document doc = null;
            try {
                if (exists(url.split("\\?")[0], j)) {
                    input = new URL(sb.toString()).openStream();
                    doc = Jsoup.parse(input, "ISO-8859-2", "http://www.ratbv.ro/");
                    doc.outputSettings().charset(Charset.forName("ISO-8859-2"));
                    doc.outputSettings().escapeMode(Entities.EscapeMode.base);
                } else {
                    sb.delete(44, sb.length());
                    continue;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            sb.delete(44, sb.length());

            assert doc != null;
            if (doc.body().text().isEmpty())
                continue;

            Element h4 = doc.select("h4").first();
            Element b = doc.select("b").first();
            Element i = doc.select("i").first();

            title = h4.text();

            SimpleDateFormat df = new SimpleDateFormat("dd/mm/yy");
            try {
                date = df.parse(b.ownText());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            subtitle = i.text();

            Elements elements = doc.getElementsMatchingText("Regia Autonoma de Transport");
            for (Element e : elements) {
                contents = e.ownText();
            }

            news.add(new News(j, title, date, subtitle, contents));
        }

        for (News n : news)
            System.out.println(n.getTitle());
    }
}
