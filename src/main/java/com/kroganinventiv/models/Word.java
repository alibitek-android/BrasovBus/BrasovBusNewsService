package com.kroganinventiv.models;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by IntelliJ IDEA.
 * User: Alex Butum
 * Date: 3/30/12
 * Time: 8:53 PM
 * <p/>
 * A simple word object that contains a name and some definitions
 */

@XmlRootElement
public class Word {
    private String name;
    private String definition;

    public Word() {
    }

    public Word(String name) {
        this.name = name;
    }

    public Word(String name, String definition) {
        this.name = name;
        this.definition = definition;
    }

    public String getName() {
        return name;
    }

    public String getDefinition() {
        return definition;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
