package com.kroganinventiv.services;

import com.kroganinventiv.models.Word;
import com.kroganinventiv.persistence.InMemoryDictionary;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

/**
 * Created by IntelliJ IDEA.
 * User: Alex Butum
 * Date: 3/30/12
 * Time: 8:57 PM
 */

@Path("/dictionary")
public class DictionaryService {
    private static InMemoryDictionary dictionary = InMemoryDictionary.instance();

    /**
     * Read the word
     *
     * @param name the name of the word
     * @return a response to the caller
     */
    @GET
    @Path("{word}")
    @Produces({"application/xml", "application/json"})
    public Response get(@PathParam("word") String name) {
        Word word = dictionary.getWord(name);
        if (word == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        return Response.ok(word).build();
    }

    /**
     * Create or Update a word
     *
     * @param wordElement the word to be created or updated
     * @return a response to the caller
     */
    @PUT
    @Path("{word}")
    @Consumes({"application/xml", "application/json"})
    public Response put(JAXBElement<Word> wordElement) {
        dictionary.putWord(wordElement.getValue());
        return Response.ok().build();
    }

    /**
     * Delete a word
     *
     * @param name the word's name to be deleted
     * @return a response to the caller
     */
    @DELETE
    @Path("{word}")
    public Response delete(@PathParam("word") String name) {
        dictionary.removeWord(name);
        return Response.ok().build();
    }
}
