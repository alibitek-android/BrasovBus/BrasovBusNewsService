package com.kroganinventiv.services;

import com.kroganinventiv.models.News;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Alex Butum
 * Date: 3/21/12
 * Time: 4:42 PM
 */
@Path("/news")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf8")
public class NewsService {
    private String title;
    private Date date;
    private String subtitle;
    private String contents;
    // "ISO-8859-2"
    private static String ENCODING = "ISO-8859-2";

    @GET
    @Path("/latest")
    public News getLatestNews() {
        int latestId = 146;
        String url = "http://www.ratbv.ro/description-news.php?id=" + latestId;
        String id = url.split("\\?")[1].split("=")[1];
        InputStream input = null;
        Document doc = null;
        try {
            if (exists(url.split("\\?")[0], Integer.parseInt(id))) {
                input = new URL(url).openStream();
                doc = Jsoup.parse(input, Charset.forName(ENCODING).name(), "http://www.ratbv.ro/");
                doc.outputSettings().charset(Charset.forName(ENCODING));
                doc.outputSettings().escapeMode(Entities.EscapeMode.base);
            } else
                return new News();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        assert doc != null;
        Element h4 = doc.select("h4").first();
        Element b = doc.select("b").first();
        Element i = doc.select("i").first();

        title = h4.text();

      DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = df.parse(b.ownText());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        subtitle = i.text();

        Elements elements = doc.getElementsMatchingText("Regia Autonoma de Transport");
        for (Element e : elements) {
            contents = e.text();
        }

        return new News(latestId, title, date, subtitle, contents);
    }

    @GET
    @Path("/all")
    public List<News> getAllNews() {
        String url = "http://www.ratbv.ro/description-news.php?id=";
        StringBuilder sb = new StringBuilder(url);
        InputStream input = null;

        List<News> news = new ArrayList<News>();
        for (int j = 81; j <= 150; j++) {
            sb.append(j);

            Document doc = null;
            try {
                if (exists(url.split("\\?")[0], j)) {
                    input = new URL(sb.toString()).openConnection().getInputStream();
                    doc = Jsoup.parse(input, ENCODING, "http://www.ratbv.ro/");
                    doc.outputSettings().charset(Charset.forName(ENCODING));
                    doc.outputSettings().escapeMode(Entities.EscapeMode.base);
                } else {
                    sb.delete(44, sb.length());
                    continue;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            sb.delete(44, sb.length());

            if (doc == null || doc.body().text().isEmpty())
                continue;

            Element h4 = doc.select("h4").first();
            Element b = doc.select("b").first();
            Element i = doc.select("i").first();

            title = h4.text();

          DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            try {
                date = df.parse(b.ownText());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            subtitle = i.text();

            Elements elements = doc.getElementsContainingText("RAT");
            for (Element e : elements) {
                contents = e.ownText();
            }

            news.add(new News(j, title, date, subtitle, contents));
        }

        return news;
    }

    public static boolean exists(String urlName, int id) {
        try {
            URL url = new URL(urlName + "?id=" + id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept-Charset", "UTF-8");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK) && (con.getContentLength() != 1);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
