package com.kroganinventiv.persistence;

import com.kroganinventiv.models.Word;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Alex Butum
 * Date: 3/30/12
 * Time: 8:57 PM
 * <p/>
 * This class is just a simple substitute for what normally would be a database backend
 * i.e. just a HashMap in memory store
 * Thread-safe and lazy initialization singleton design pattern
 */
public final class InMemoryDictionary {
    private Map<String, Word> words = new HashMap<String, Word>();

    private static InMemoryDictionary INSTANCE = null;

    private InMemoryDictionary() {
        initialize();
    }

    /**
     * Get an instance of this class
     *
     * @return a new instance of the class
     */
    public static InMemoryDictionary instance() {
        if (INSTANCE == null)
            INSTANCE = new InMemoryDictionary();

        return INSTANCE;
    }

    /**
     * Populate the dictionary with some initial values
     */
    private void initialize() {
        Word set = new Word("set");
        set.setDefinition("A collection that contains no duplicate elements");

        Word list = new Word("list");
        list.setDefinition("An ordered collection (also known as a sequence)");

        putWord(set);
        putWord(list);
    }

    /**
     * Gets all the words in the dictionary
     *
     * @return a <code>Map</code> with all the words in the dictionary
     */
    public Map getWords() {
        return Collections.unmodifiableMap(words);
    }

    /**
     * Get the definition of a word
     *
     * @param wordName the word for which the definition is requested
     * @return if the word is in the dictionary, a <code>List</code> of
     */
    public Word getWord(String wordName) {
        return words.get(wordName);
    }

    /**
     * Adds a new word in the dictionary
     *
     * @param word the word to be added to the dictionary
     */
    public void putWord(Word word) {
        words.put(word.getName(), word);
    }

    /**
     * Remove a word from the dictionary
     *
     * @param wordName the word to be removed from the dictionary
     */
    public void removeWord(String wordName) {
        words.remove(wordName);
    }
}
